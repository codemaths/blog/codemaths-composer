pipeline {
    agent any
    options {
        buildDiscarder(logRotator(numToKeepStr:'30'))
    }
    stages{
        stage('Clone repository') {
            steps {
                git branch: '${GIT_BRANCH}', url: '${GIT_REPO_URL}'
            }

        }

        stage('Compying YAML file into the box') {
            steps {
                sh 'scp ./docker-compose.yml jenkins@${TARGET_HOST}:/home/jenkins/docker-compose.yml'
            }
        }

        stage('Print working directory') {
            steps {
                sh 'ssh jenkins@${TARGET_HOST} "pwd"'
                sh 'ssh jenkins@${TARGET_HOST} "ls -la"'
            }
        }

        stage('Show running containers'){
            steps {
                sh 'ssh jenkins@${TARGET_HOST} "docker-compose ps -a"'
            }
        }

        stage('Stop spcific container') {
            when {
                expression {
                    params.CONTAINER_NAME != ''
                }
            }
            steps {
                sh 'ssh jenkins@${TARGET_HOST} " docker-compose ps -q --filter \"name=${CONTAINER_NAME}\" | grep -q . && docker-compose stop ${CONTAINER_NAME}"'
            }
        }

        stage('Stop all containers') {
            when {
                expression {
                    params.CONTAINER_NAME == ''
                }
            }
            steps{
                sh 'ssh jenkins@${TARGET_HOST} "docker-compose down"'
            }
        }

        stage('Remove stopped service containers') {
            steps{
                sh 'ssh jenkins@${TARGET_HOST} "docker-compose rm -f"'
            }
        }

        stage('Pull the latest images') {
            steps {
                sh 'ssh jenkins@${TARGET_HOST} "docker-compose pull ${CONTAINER_NAME}"'
            }
        }

        stage('Build environment') {
            steps{
                sh 'ssh jenkins@${TARGET_HOST} "echo "ENVIRONMENT=${ENVIRONMENT}"  > .env"'
                sh 'ssh jenkins@${TARGET_HOST} "echo "POSTGRES_USER=${POSTGRES_USER}" >> .env"'
                sh 'ssh jenkins@${TARGET_HOST} "echo "POSTGRES_PASSWORD=${POSTGRES_PASSWORD}" >> .env"'
                sh 'ssh jenkins@${TARGET_HOST} "echo "POSTGRES_DB=${POSTGRES_DB}" >> .env"'
                sh 'ssh jenkins@${TARGET_HOST} "echo "POSTGRES_HOST=${POSTGRES_HOST}" >> .env"'
                sh 'ssh jenkins@${TARGET_HOST} "echo "DOMAIN_NAME=${DOMAIN_NAME}" >> .env"'
            }
        }

        stage('Starts the container') {
            steps{
                sh 'ssh jenkins@${TARGET_HOST} "docker-compose up -d ${CONTAINER_NAME}"'
            }
        }
    }
}